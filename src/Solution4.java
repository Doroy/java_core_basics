import java.util.Arrays;
public class Solution4 {
    public static void main(String[] args) {
        double[] array = new double[20];
        for (int i = 0; i<array.length; i++) {
            array[i] = (Math.random() * 20);
        }
        System.out.println(Arrays.toString(array));
        findMax(array);
        findMin(array);
    }

    public static void findMax(double[] array) {
        double max = array[0];
        for (int i = 0; i <array.length ; i++) {
            if (array[i]>array[0])
                max = array[i];

        };
        System.out.println(max);
    }
    public static void findMin(double[] array) {
        double min = array[0];
        for (int i = 0; i < array.length; i++) {
            if (array[i] < array[0]) {
                min = array[i];
            }
        }
        System.out.println(min);
    }
}
